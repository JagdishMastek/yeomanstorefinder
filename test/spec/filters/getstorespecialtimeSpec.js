/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Filter: getStoreSpecialTime', function () {

    // load the filter's module
    beforeEach(module('storesApp.filters.GetStoreSpecialTime'));

    // initialize a new instance of the filter before each test
    var getStoreSpecialTime;
    beforeEach(inject(function ($filter) {
      getStoreSpecialTime = $filter('getStoreSpecialTime');
    }));

    it('should return the input prefixed with "getStoreSpecialTime filter:"', function () {
      var text = 'angularjs';
      expect(getStoreSpecialTime(text)).toBe('getStoreSpecialTime filter: ' + text);
    });

  });
});
