/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Filter: getFreshStore', function () {

    // load the filter's module
    beforeEach(module('storesApp.filters.GetFreshStore'));

    // initialize a new instance of the filter before each test
    var getFreshStore;
    beforeEach(inject(function ($filter) {
      getFreshStore = $filter('getFreshStore');
    }));

    it('should return the input prefixed with "getFreshStore filter:"', function () {
      var text = 'angularjs';
      expect(getFreshStore(text)).toBe('getFreshStore filter: ' + text);
    });

  });
});
