/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Filter: openingClosing', function () {

    // load the filter's module
    beforeEach(module('storesApp.filters.OpeningClosing'));

    // initialize a new instance of the filter before each test
    var openingClosing;
    beforeEach(inject(function ($filter) {
      openingClosing = $filter('openingClosing');
    }));

    it('should return the input prefixed with "openingClosing filter:"', function () {
      var text = 'angularjs';
      expect(openingClosing(text)).toBe('openingClosing filter: ' + text);
    });

  });
});
