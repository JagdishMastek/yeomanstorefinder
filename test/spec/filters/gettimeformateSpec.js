/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Filter: getTimeFormate', function () {

    // load the filter's module
    beforeEach(module('storesApp.filters.GetTimeFormate'));

    // initialize a new instance of the filter before each test
    var getTimeFormate;
    beforeEach(inject(function ($filter) {
      getTimeFormate = $filter('getTimeFormate');
    }));

    it('should return the input prefixed with "getTimeFormate filter:"', function () {
      var text = 'angularjs';
      expect(getTimeFormate(text)).toBe('getTimeFormate filter: ' + text);
    });

  });
});
