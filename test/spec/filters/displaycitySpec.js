/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Filter: displayCity', function () {

    // load the filter's module
    beforeEach(module('storesApp.filters.DisplayCity'));

    // initialize a new instance of the filter before each test
    var displayCity;
    beforeEach(inject(function ($filter) {
      displayCity = $filter('displayCity');
    }));

    it('should return the input prefixed with "displayCity filter:"', function () {
      var text = 'angularjs';
      expect(displayCity(text)).toBe('displayCity filter: ' + text);
    });

  });
});
