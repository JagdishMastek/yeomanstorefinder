/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Filter: getStoreTime', function () {

    // load the filter's module
    beforeEach(module('storesApp.filters.GetStoreTime'));

    // initialize a new instance of the filter before each test
    var getStoreTime;
    beforeEach(inject(function ($filter) {
      getStoreTime = $filter('getStoreTime');
    }));

    it('should return the input prefixed with "getStoreTime filter:"', function () {
      var text = 'angularjs';
      expect(getStoreTime(text)).toBe('getStoreTime filter: ' + text);
    });

  });
});
