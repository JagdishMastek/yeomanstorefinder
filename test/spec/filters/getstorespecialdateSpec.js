/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Filter: getStoreSpecialDate', function () {

    // load the filter's module
    beforeEach(module('storesApp.filters.GetStoreSpecialDate'));

    // initialize a new instance of the filter before each test
    var getStoreSpecialDate;
    beforeEach(inject(function ($filter) {
      getStoreSpecialDate = $filter('getStoreSpecialDate');
    }));

    it('should return the input prefixed with "getStoreSpecialDate filter:"', function () {
      var text = 'angularjs';
      expect(getStoreSpecialDate(text)).toBe('getStoreSpecialDate filter: ' + text);
    });

  });
});
