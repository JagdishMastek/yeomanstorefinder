/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Directive: bingMapStore', function () {

    // load the directive's module
    beforeEach(module('storesApp.directives.BingMapStore'));

    var element,
      scope;

    beforeEach(inject(function ($rootScope) {
      scope = $rootScope.$new();
    }));

    it('should make hidden element visible', inject(function ($compile) {
      element = angular.element('<bing-map-store></bing-map-store>');
      element = $compile(element)(scope);
      expect(element.text()).toBe('this is the bingMapStore directive');
    }));
  });
});
