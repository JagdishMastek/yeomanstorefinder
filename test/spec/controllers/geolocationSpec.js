/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Controller: GeoLocationCtrl', function () {

    // load the controller's module
    beforeEach(module('storesApp.controllers.GeoLocationCtrl'));

    var GeoLocationCtrl;
    var scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
      scope = $rootScope.$new();
      GeoLocationCtrl = $controller('GeoLocationCtrl', {
        $scope: scope
        // place here mocked dependencies
      });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
      expect(GeoLocationCtrl.awesomeThings.length).toBe(3);
    });
  });
});
