/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Controller: StoreConfirmCtrl', function () {

    // load the controller's module
    beforeEach(module('storesApp.controllers.StoreConfirmCtrl'));

    var StoreConfirmCtrl;
    var scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
      scope = $rootScope.$new();
      StoreConfirmCtrl = $controller('StoreConfirmCtrl', {
        $scope: scope
        // place here mocked dependencies
      });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
      expect(StoreConfirmCtrl.awesomeThings.length).toBe(3);
    });
  });
});
