/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Controller: StoreSearchCtrl', function () {

    // load the controller's module
    beforeEach(module('storesApp.controllers.StoreSearchCtrl'));

    var StoreSearchCtrl;
    var scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
      scope = $rootScope.$new();
      StoreSearchCtrl = $controller('StoreSearchCtrl', {
        $scope: scope
        // place here mocked dependencies
      });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
      expect(StoreSearchCtrl.awesomeThings.length).toBe(3);
    });
  });
});
