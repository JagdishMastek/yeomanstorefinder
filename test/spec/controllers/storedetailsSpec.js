/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Controller: StoreDetailsCtrl', function () {

    // load the controller's module
    beforeEach(module('storesApp.controllers.StoreDetailsCtrl'));

    var StoreDetailsCtrl;
    var scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
      scope = $rootScope.$new();
      StoreDetailsCtrl = $controller('StoreDetailsCtrl', {
        $scope: scope
        // place here mocked dependencies
      });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
      expect(StoreDetailsCtrl.awesomeThings.length).toBe(3);
    });
  });
});
