/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Controller: BingShowRouteCtrl', function () {

    // load the controller's module
    beforeEach(module('storesApp.controllers.BingShowRouteCtrl'));

    var BingShowRouteCtrl;
    var scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
      scope = $rootScope.$new();
      BingShowRouteCtrl = $controller('BingShowRouteCtrl', {
        $scope: scope
        // place here mocked dependencies
      });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
      expect(BingShowRouteCtrl.awesomeThings.length).toBe(3);
    });
  });
});
