/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: bingFactory', function () {

    // load the service's module
    beforeEach(module('storesApp.services.BingFactory'));

    // instantiate service
    var bingFactory;
    beforeEach(inject(function (_bingFactory_) {
      bingFactory = _bingFactory_;
    }));

    it('should do something', function () {
      expect(!!bingFactory).toBe(true);
    });

  });
});
