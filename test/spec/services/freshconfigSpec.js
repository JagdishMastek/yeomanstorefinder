/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: freshConfig', function () {

    // load the service's module
    beforeEach(module('storesApp.services.FreshConfig'));

    // instantiate service
    var freshConfig;
    beforeEach(inject(function (_freshConfig_) {
      freshConfig = _freshConfig_;
    }));

    it('should do something', function () {
      expect(!!freshConfig).toBe(true);
    });

  });
});
