/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: config', function () {

    // load the service's module
    beforeEach(module('storesApp.services.Config'));

    // instantiate service
    var config;
    beforeEach(inject(function (_config_) {
      config = _config_;
    }));

    it('should do something', function () {
      expect(!!config).toBe(true);
    });

  });
});
