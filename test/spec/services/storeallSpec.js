/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: storeAll', function () {

    // load the service's module
    beforeEach(module('storesApp.services.StoreAll'));

    // instantiate service
    var storeAll;
    beforeEach(inject(function (_storeAll_) {
      storeAll = _storeAll_;
    }));

    it('should do something', function () {
      expect(!!storeAll).toBe(true);
    });

  });
});
