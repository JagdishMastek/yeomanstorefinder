/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: storeHighLighted', function () {

    // load the service's module
    beforeEach(module('storesApp.services.StoreHighLighted'));

    // instantiate service
    var storeHighLighted;
    beforeEach(inject(function (_storeHighLighted_) {
      storeHighLighted = _storeHighLighted_;
    }));

    it('should do something', function () {
      expect(!!storeHighLighted).toBe(true);
    });

  });
});
