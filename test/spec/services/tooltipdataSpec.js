/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: toolTipData', function () {

    // load the service's module
    beforeEach(module('storesApp.services.ToolTipData'));

    // instantiate service
    var toolTipData;
    beforeEach(inject(function (_toolTipData_) {
      toolTipData = _toolTipData_;
    }));

    it('should do something', function () {
      expect(!!toolTipData).toBe(true);
    });

  });
});
