/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: geolocation', function () {

    // load the service's module
    beforeEach(module('storesApp.services.Geolocation'));

    // instantiate service
    var geolocation;
    beforeEach(inject(function (_geolocation_) {
      geolocation = _geolocation_;
    }));

    it('should do something', function () {
      expect(!!geolocation).toBe(true);
    });

  });
});
