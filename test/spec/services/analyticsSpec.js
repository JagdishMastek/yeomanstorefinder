/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: Analytics', function () {

    // load the service's module
    beforeEach(module('storesApp.services.Analytics'));

    // instantiate service
    var Analytics;
    beforeEach(inject(function (_Analytics_) {
      Analytics = _Analytics_;
    }));

    it('should do something', function () {
      expect(!!Analytics).toBe(true);
    });

  });
});
