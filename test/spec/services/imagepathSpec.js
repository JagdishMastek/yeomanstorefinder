/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: imagePath', function () {

    // load the service's module
    beforeEach(module('storesApp.services.ImagePath'));

    // instantiate service
    var imagePath;
    beforeEach(inject(function (_imagePath_) {
      imagePath = _imagePath_;
    }));

    it('should do something', function () {
      expect(!!imagePath).toBe(true);
    });

  });
});
