/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: store', function () {

    // load the service's module
    beforeEach(module('storesApp.services.Store'));

    // instantiate service
    var store;
    beforeEach(inject(function (_store_) {
      store = _store_;
    }));

    it('should do something', function () {
      expect(!!store).toBe(true);
    });

  });
});
