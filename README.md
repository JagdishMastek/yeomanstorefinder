# Store Finder README #

Morrisons Store Finder application.

[my.morrisons.com/storefinder](https://my.morrisons.com/storefinder/)

## Dependencies ##

* NPM - [Download](https://nodejs.org/en/download/)
* Grunt must be installed globally
* Bower must be installed globally
* Ruby Gem - [Download](https://nodejs.org/en/download/)

## Setup ##

Install `grunt-cli` and `bower` using terminal command:

    npm install -g grunt-cli bower


#### Ruby Dependencies ####
After [Ruby Gem](https://nodejs.org/en/download/) is installed, open the Command Prompt with Ruby.

Run command `gem install ruby`

And then install compass `gem install compass`

## Install ##

Clone the repo
`git clone https://ArifUllah@bitbucket.org/morrisonsplc/storefinder-yeoman.git`

CD to cloned directory
`cd path/to/directory`

Run the following commands to install dependencies

    npm install
    bower install


## Run application ##

* Run `grunt` for building and `grunt serve` for preview

## Build for Prod ##

Open **config.js** located in `scripts/services/config.js`

Find the code:


```
#!javascript

configModule.constant('cfg', { fresh_look_store: [11, 89, 137, 33], 'bing_api_key': 'Ag00iheqslugEVbwF43XtJCoY0tAUdcyXd8XS3oA4l389lpCxFj2vYXknu_B_0ua', 'stores_api_key': 'Avdsv8PKMF8tjAkYJYYl1QE4SZBlYJKJ' });
```


Change the relevant API keys

* Bing maps: `bing_api_key`
* Stores: `stores_api_key`

### Build the final application ###
* Run `grunt build` to build the application ready for deployment.  

The build files are created in the 'dist' directory.

Open **index.html** and change 
```
#!html

<base href="/">
```
to

```
#!html

<base href="/storefinder/">
```

The application is now ready to deploy.