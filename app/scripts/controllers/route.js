define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name storesApp.controller:RouteCtrl
   * @description
   * # RouteCtrl
   * Controller of the storesApp
   */
  angular.module('storesApp.controllers.RouteCtrl', [])
    .controller('RouteCtrl', function ($scope,$location,bingFactory,LocalStorageService,geolocation,$routeParams) {
      $scope.getRoute = function(){
        if ($scope.routeForm.$valid) {
           bingFactory.get($scope.travelFrom).success(function (response) {
             LocalStorageService.clearAll();
             var dbResult = response.resourceSets[0].resources;
             if(dbResult.length===1){
              if (dbResult[0].point.coordinates[0] === "54.56088638305664" && dbResult[0].point.coordinates[1] === "-2.2125117778778076"){
                  $scope.directionsError = true;
                  $scope.directionsErrorMsg  = "The postcode seems invalid. Please try again."; 
              }else{
                   $scope.lat = dbResult[0].point.coordinates[0];
                   $scope.long = dbResult[0].point.coordinates[1];
                   LocalStorageService.setItem('cachedLatitude', $scope.lat);  
                   LocalStorageService.setItem('cachedLongitude',$scope.long);  
                   $location.path($routeParams.storeId + "/" + $scope.travelFrom);
              }
            }
            else{    
              $scope.directionsError = true;
              $scope.directionsErrorMsg  = "We could not determine your start location. Please provide a valid postcode."; 
            }
           })
           .error( function (response) {
              //console.log(response);
              $scope.directionsError = true;
              $scope.directionsErrorMsg = "Something went wrong, please try again";
           }); 
        }
      };

      $scope.captureUserLocation = function(){
        angular.element( document.querySelector( '#routeUserLoc' ) ).removeClass("current-location-icon").addClass("loader");   
        geolocation.getLocation().then(function(result){
                LocalStorageService.clearAll();
                angular.element( document.querySelector( '#routeUserLoc' ) ).removeClass("loader").addClass("current-location-icon");   
                $scope.location = result;
                if ($scope.location.longitude === "54.56088638305664" && $scope.location.longitude === "-2.2125117778778076"){
                  $scope.directionsError = true;
                  $scope.directionsErrorMsg  = "The postcode seems invalid. Please try again."; 
                }
                else{
                  LocalStorageService.setItem('cachedLatitude', $scope.location.latitude);  
                  LocalStorageService.setItem('cachedLongitude',$scope.location.longitude); 
                  $location.path($routeParams.storeId + "/user-location");
                }
                }, function(error) {                 
                          $('#routeUserLoc').removeClass("loader").addClass("current-location-icon");  
                          $scope.progressbar.complete(); 
                          $scope.directionsError = true;
                          //console.log(error);
                          var msg = "";
                          if (error.code == 0) {
                              msg = "Browser does not support location services";
                          }
                          if (error.code == 1) {
                              msg = "Access denied by user";
                          }
                          if (error.code == 2) {
                              msg = "Unable to determine your location";
                          }
                          if (error.code == 3) {
                              msg ="Service timeout has been reached";
                          }
                          
                          $scope.directionsErrorMsg = msg;
                      });
      };
    });
});
