define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name storesApp.controller:StoreRouteCtrl
   * @description
   * # StoreRouteCtrl
   * Controller of the storesApp
   */
  angular.module('storesApp.controllers.StoreRouteCtrl', [])
    .controller('StoreRouteCtrl', function ($scope,$routeParams,store,LocalStorageService,cfg,$compile,ngMeta,ngProgressFactory,bingFactory) {
        $scope.progressbar = ngProgressFactory.createInstance();
        $scope.progressbar.start();
        store.getStore({dest:'stores',storeId:$routeParams.storeId,apikey:cfg.stores_api_key}).$promise.then(function (storeData) {
         
          ngMeta.setTitle('Find the quickest route to Morrisons ' + storeData.storeName);  
          ngMeta.setDefaultTag('keywords', 'Supermarket,Petrol station,Cafe');
          $scope.selectedStore = storeData;
          $scope.storeName = storeData.storeName;
          $scope.storeId=$routeParams.storeId;
          $scope.latitudeTo =  storeData.location.latitude;
          $scope.longitudeTo =  storeData.location.longitude;
          
          if($routeParams.travelFrom!="user-location"){
              //console.log("i m here");
              bingFactory.get($routeParams.travelFrom).success(function (response) {
                var resultLen = response.resourceSets[0].resources.length;
                var resultset = response.resourceSets[0].resources;
                if(resultset){
                    var result = resultset;
                    //console.log(JSON.stringify(result));
                    angular.forEach(result, function(list) {
                        $scope.selectedLat = list.point.coordinates[0];
                        $scope.selectedlog = list.point.coordinates[1];
                    });
                    LocalStorageService.removeItem('cachedLatitude');
                    LocalStorageService.removeItem('cachedLongitude');
                    LocalStorageService.setItem('cachedLatitude', $scope.selectedLat);  
                    LocalStorageService.setItem('cachedLongitude',$scope.selectedlog); 
                    
                }
                })
                .error( function (response) {
                    //console.log(response);
                    $scope.progressbar.complete();
                    $scope.routeError=true;
                    $scope.routeErrorMsg = "We could not determine your start location. Please provide a valid postcode.";
                    

                }); 

          }
          var latitudeFrom = LocalStorageService.getItem('cachedLatitude');
          var longitudeFrom = LocalStorageService.getItem('cachedLongitude');
          if(latitudeFrom === "null" && longitudeFrom === "null"){
              $scope.progressbar.complete();
              $scope.routeError=true;
              $scope.routeErrorMsg = "We could not determine your start location. Please provide a valid postcode.";
          }
          else{
              $scope.latitudeFrom = LocalStorageService.getItem('cachedLatitude');
              $scope.longitudeFrom = LocalStorageService.getItem('cachedLongitude');
          }

          $scope.bingMapArgs = {  
              latitudeto: $scope.latitudeTo,  
              longitudeto:$scope.longitudeTo,
              latitudefrom:$scope.latitudeFrom,
              longitudefrom:$scope.longitudeFrom
          } 
           
          //$scope.routeMap(storeData);
           $scope.progressbar.complete();
        });
    });
});
