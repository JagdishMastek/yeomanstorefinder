define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name storesApp.controller:StoreSearchCtrl
   * @description
   * # StoreSearchCtrl
   * Controller of the storesApp
   */
  angular.module('storesApp.controllers.StoreSearchCtrl', [])
    .controller('StoreSearchCtrl', function ($scope,$rootScope,$location,bingFactory,LocalStorageService,geolocation,ngMeta,$timeout,storeAll,cfg,ngProgressFactory) {
      
    //Page Title & meta tag 
    ngMeta.setTitle('Morrisons Store Locator | Find Your Nearest Morrisons Supermarket');     
    //ngMeta.setDefaultTag('keywords', 'Supermarket,Petrol station,Cafe');
    ngMeta.setTag('description', 'Find your nearest Morrisons supermarket and its opening times, address and phone number using our store locator, plus see what services are available.');
    angular.element( document.querySelector( '.search > div, .search form > div' ) ).addClass("form-spacing");   
          //$('.search > div, .search form > div').addClass("form-spacing");
     $scope.progressbar = ngProgressFactory.createInstance();   
       
         $scope.findStore = function(){
           var locationErr = false;
            if (this.storeForm.$valid) { 
               
                $location.path('/').replace();
                $scope.progressbar.start();    
                angular.element( document.querySelector( '.search > div, .search form > div' ) ).removeClass("form-spacing");                  
                
                $scope.searchTerm = this.searchTerm;
                
                bingFactory.get($scope.searchTerm).success(function (response) {
                  var resultLen = response.resourceSets[0].estimatedTotal;
                  if(resultLen > 0){
                  var resultData = response.resourceSets[0].resources;
                    if(resultLen!=1){
                      var result = resultData;
                      var selectedCity_scope = [];
                      angular.forEach(result, function(list) {
                         
                          var searchPlace = list.name;
                          var searchCity = searchPlace.replace(", United Kingdom", "");
                          searchCity = searchCity.replace(" ", "");
                          var obj = {"city":list.name,"cityLatlong":list.point.coordinates[0]+"::"+list.point.coordinates[1],"searchCity":searchCity};
                          selectedCity_scope.push(obj);
                      }); 
                      $scope.locationErr = false;  
                      $rootScope.dbCityList = selectedCity_scope;
                      angular.element( document.querySelector( '.search > div, .search form > div' ) ).removeClass("form-spacing");   
                      angular.element( document.querySelector( '#storeCityListingDiv' ) ).removeClass("hide");    
                      angular.element( document.querySelector( '#storeListingDiv' ) ).removeClass("show").addClass("hide");
                      $scope.progressbar.complete();
   
                    }
                    else{ 
                      $scope.progressbar.complete();                     
                      var latlong = resultData[0].point.coordinates[0]+"::"+resultData[0].point.coordinates[1];
                      $location.path("stores/"+$scope.searchTerm);
                    }
                  }
                  else{
                    $scope.progressbar.complete(); 
                    $scope.locationErr = true;
                    $scope.errorMsg = "No stores found, please try again";
                  }
                  })
                .error( function (response) {
                      $scope.progressbar.complete(); 
                      $scope.locationErr = true;
                      $scope.errorMsg = "Sorry, something going wrong, please try again";

                }); 
            }
            else{
               //LocalStorageService.clearAll();
                $scope.progressbar.complete(); 
                $scope.locationErr = true;
                $scope.errorMsg = "No stores found, please try again";
                
            }
          }

          $scope.searchByCurrent = function(){
              $location.path('/').replace();
              $scope.progressbar.start();  
              angular.element( document.querySelector( '#storeCityListingDiv' ) ).removeClass("show").addClass("hide");
              angular.element( document.querySelector( '#storeListingDiv' ) ).removeClass("show").addClass("hide");
            
              $('#curentLoc').removeClass("current-location-icon").addClass("loader");
                geolocation.getLocation().then(function(result){
                  $('#curentLoc').removeClass("loader").addClass("current-location-icon");  
                  $scope.location = result;
                  var latlong = $scope.location.latitude+"::"+$scope.location.longitude;
                  LocalStorageService.removeItem("ResultLatLong");
                  LocalStorageService.setItem("ResultLatLong",latlong);
                  $scope.progressbar.complete(); 
                  $location.path("stores/user-location");
                  
               }, function(error) {
           //unable to get coordinates
                  $('#curentLoc').removeClass("loader").addClass("current-location-icon");  
                  $scope.progressbar.complete(); 
                  $scope.locationErr = true;
                  //console.log(error);
                  var msg = "";
                  if (error.code == 0) {
                      msg = "Browser does not support location services";
                  }
                  if (error.code == 1) {
                      msg = "Access denied by user";
                  }
                  if (error.code == 2) {
                      msg = "Unable to determine your location";
                  }
                  if (error.code == 3) {
                      msg ="Service timeout has been reached";
                  }
                  
                  $scope.errorMsg = msg;
              });
          };
      
    });
});
