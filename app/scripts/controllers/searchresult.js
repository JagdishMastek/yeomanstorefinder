define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name storesApp.controller:SearchResultCtrl
   * @description
   * # SearchResultCtrl
   * Controller of the storesApp
   */
  angular.module('storesApp.controllers.SearchResultCtrl', [])
    .controller('SearchResultCtrl', function ($scope, $routeParams, $http, storeAll, cfg, $window, ngMeta, LocalStorageService,bingFactory,$timeout,ngProgressFactory) {
      $scope.progressbar = ngProgressFactory.createInstance();
      $scope.progressbar.start();
      //Page Title & meta tag
      ngMeta.setTitle('Morrisons Store Locator | Find Your Nearest Morrisons Supermarket');
      //ngMeta.setDefaultTag('keywords', 'Supermarket,Petrol station,Cafe');
      ngMeta.setTag('description', 'Find your nearest Morrisons supermarket and its opening times, address and phone number using our store locator, plus see what services are available.');

      $scope.locationErr = false;
      if($routeParams.searchTerm==="user-location"){
        var latlog = LocalStorageService.getItem("ResultLatLong");//$routeParams.latlang;
        $scope.latlog = latlog;
        var latlonArray = latlog.split('::');
        //console.log(latlonArray);
        $scope.selectedLat = latlonArray[0];
        $scope.selectedlog = latlonArray[1];
        viewStores($scope.selectedLat,$scope.selectedlog);
      }
      else{
        if(encodeURIComponent($routeParams.searchTerm)){
          bingFactory.get($routeParams.searchTerm).success(function (response) {
              var resultLen = response.resourceSets[0].estimatedTotal;
              var resultset = response.resourceSets[0].resources;
              if(resultset){
                  var result = resultset;

                  angular.forEach(result, function(list) {
                    $scope.selectedLat = list.point.coordinates[0];
                    $scope.selectedlog = list.point.coordinates[1];
                  });
                  viewStores($scope.selectedLat,$scope.selectedlog);
              }
            })
            .error( function (response) {
                 //console.log(response);
                 $scope.progressbar.complete();
                 $scope.locationErr = true;
                 $scope.errorMsg = "Something went wrong, please try again";


            });
        }
     }

       function viewStores (latitude,longitude){
         if(latitude == "54.56088638305664" && longitude == "-2.2125117778778076") {
             $scope.progressbar.complete();
             $scope.locationErr = true;
             $scope.errorMsg = "Sorry that's an unknown location, please try again";
             return;
          }
          storeAll.getNestedData({dest:'stores',apikey:cfg.stores_api_key,distance:50000,lat:encodeURIComponent(latitude),limit:10,lon:encodeURIComponent(longitude),offset:0,storeformat:'supermarket'}).then(function(result) {
            var dbMarkers_scope = [];
            $scope.dbMarkers = {};
            var latlong = latitude+"::"+longitude;
            LocalStorageService.setItem("ResultLatLong",latlong)
            //$('.search > div, .search form > div').removeClass("form-spacing");
            angular.forEach(result, function(records) {

                var store = records.stores;
                var storeSpecialTime = records.specialtime;
                var obj = {"id":store.name,"storeName":store.storeName,"latitude":store.location.latitude,"longitude":store.location.longitude,"distance":store.distance,"telephone":store.telephone,"postcode":store.address.postcode,"city":store.address.city,"county":store.address.county,"addressLine1":store.address.addressLine1,"addressLine2": store.address.addressLine2,'storeTiming':store.openingTimes,"specialTime":storeSpecialTime};

                dbMarkers_scope.push(obj);
            });
            if(dbMarkers_scope.length==0){
                $scope.locationErr = true;
                $scope.progressbar.complete();
                $scope.errorMsg = "No stores found, please try again";
            }else{
                angular.element( document.querySelector( '.search > div, .search form > div' ) ).removeClass("form-spacing");
                $scope.dbMarkers = dbMarkers_scope;
                $scope.progressbar.complete();

                //$scope.getMap(dbMarkers_scope);
                $scope.mapOptions = {
                    credentials: cfg.bing_api_key ,
                    center : new Microsoft.Maps.Location($scope.selectedLat, $scope.selectedlog),
                    zoom :10,
                    showMapTypeSelector:true,
                    enableHighDpi: true,
                    showCopyright: false,
                    enableSearchLogo: false,
                    enableClickableLogo: false,
                    backgroundColor:new Microsoft.Maps.Color.fromHex("#ffffff"),
                    animate: false
              };

            }
          }, function (error) {
                  //alert("error");
                  $scope.progressbar.complete();
                  $scope.locationErr = true;
                  $scope.errorMsg = "Something went wrong, please try again";

          });

   }

    });

});
