define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name storesApp.controller:StoreDetailsCtrl
   * @description
   * # StoreDetailsCtrl
   * Controller of the storesApp
   */
  angular.module('storesApp.controllers.StoreDetailsCtrl', [])
    .controller('StoreDetailsCtrl', function ($scope, $routeParams, $sce, imagePath, toolTipData , store, $filter, getTimeFormateFilter, $window, $location, $browser, storeHighLighted, orderByFilter, cfg, bingFactory, $q, ngMeta, ngProgressFactory,localDeliverData) {
      $window.scrollTo(0, 0);

      $scope.progressbar = ngProgressFactory.createInstance();
      $scope.progressbar.start();
      $scope.data1 = store.getStore({dest:'stores',storeId:$routeParams.storeId,apikey:cfg.stores_api_key,include:'departments,services,linkedStores'});
      $scope.data2 = store.getStoreOpening({dest:'stores',storeId:$routeParams.storeId,openingtime:'specialopeningtimes',apikey:cfg.stores_api_key});
      var data = $q.all({storeData: $scope.data1.$promise,storeSpecialTime:$scope.data2.$promise}).then(function (values) {
                var storeData = values.storeData;
                var storeSpecialTiming = values.storeSpecialTime;
                $scope.bingApiKey = cfg.bing_api_key;
                var selectedStore_scope = $scope.selectedStore;
                var services_scope = $scope.services;
                var depart_scope = $scope.depart_scope;
                var linkedLocations_scope = $scope.linkedLocations_scope;
                var tabData = new Array();
                $scope.absurl = $location.absUrl();
                selectedStore_scope = storeData;
                selectedStore_scope.name = storeData.name;
                selectedStore_scope.storeName = storeData.storeName;
                selectedStore_scope.storeFormat = storeData.storeFormat;
                selectedStore_scope.category = storeData.category;
                selectedStore_scope.region = storeData.region;
                selectedStore_scope.telephone = storeData.telephone;
                selectedStore_scope.address.addressLine1 = storeData.address.addressLine1;
                selectedStore_scope.address.addressLine2 = storeData.address.addressLine2;
                selectedStore_scope.address.county = storeData.address.county;
                selectedStore_scope.address.city = storeData.address.city;
                selectedStore_scope.address.postcode = storeData.address.postcode;
                selectedStore_scope.address.country = storeData.address.country;
                selectedStore_scope.location.latitude = storeData.location.latitude;
                selectedStore_scope.location.longitude = storeData.location.longitude;
                selectedStore_scope.satnav.latitude = storeData.satnav.latitude;
                selectedStore_scope.satnav.longitude = storeData.satnav.longitude;
                $scope.directionsError = false;
                $scope.travelFrom = $routeParams.travelFrom;
                selectedStore_scope.storeSpecialTiming = storeSpecialTiming;
                var url = $location.protocol()+"://"+$location.host()+$browser.baseHref();
                selectedStore_scope.storeDefaultImage = './images/store-images/generic-store-header.jpg';
                selectedStore_scope.storeImage = './images/store-images/'+storeData.name+'.jpg';
                var defultOpeningTime = storeData.openingTimes;
                var tabservice = new Array();
                var storesOpeningTimes = new Array();
                tabservice.push('Store');
                var obj = {name:"supermarket","openingTime":defultOpeningTime,"displayname":"Store"};
                storesOpeningTimes.push(obj);
                tabData.push(defultOpeningTime);
                ngMeta.setTitle('Morrisons '+ storeData.storeName+' Store Details');
                var metaDescr = "Address, phone number and opening times for Morrisons "+storeData.storeName+". Find out what services are available in your local Morrisons "+ storeData.storeFormat+".";
                ngMeta.setDefaultTag('keywords', 'Supermarket,Petrol station,Cafe');
                ngMeta.setTag('description', metaDescr);
                //$scope.bingMap(storeData);
                /****************************************************
                 * Set Bing Map options
                 *
                 ****************************************************/
                 var customIcon = "./images/ListingsPin-01.svg";
                 $scope.mapOptions = {
                  credentials: cfg.bing_api_key,
                  center : new Microsoft.Maps.Location(storeData.location.latitude,
                  storeData.location.longitude),
                  zoom :15,
                  showDashboard: true,
                  showMapTypeSelector:true,
                  enableHighDpi: true,
                  showCopyright:false,
                  enableSearchLogo: false,
                  enableClickableLogo: false
                }

                /* Getting the store services */
                var servicesArr = [];
                services_scope = [];
                angular.forEach(storeData.services, function(records) {
                  var obj = {"name":records.name,"serviceName":records.serviceName,"displayImage":$sce.trustAsResourceUrl(imagePath(records.name)),"toolTipe":toolTipData(records.name)};
                  services_scope.push(obj);
                  servicesArr.push(records.name);
                });

                /* Getting the linked location */
                linkedLocations_scope = [];
                angular.forEach(storeData.linkedLocations, function(recordLocations) {
                   var  storeNames = "";
                  if(recordLocations.storeFormat=="pfs"){
                        storeNames ="petrolstation";

                    }
                    else{
                        storeNames = recordLocations.serviceName;

                    }
                    var obj = {"serviceName":storeNames,"displayImage":$sce.trustAsResourceUrl(imagePath(recordLocations.storeFormat)),"toolTipe":toolTipData(recordLocations.name)};
                    linkedLocations_scope.push(obj);
                      if(recordLocations.openingTimes){
                        tabservice.push(storeNames);
                        var obj = {name:recordLocations.storeFormat.toLowerCase(),"openingTime":recordLocations.openingTimes,"displayname":storeNames};
                        storesOpeningTimes.push(obj);
                        tabData.push(recordLocations.openingTimes);
                      }
                });



                depart_scope = [];
                angular.forEach(storeData.departments, function(departments) {
                    var obj = {"serviceName":departments.serviceName,"displayImage":$sce.trustAsResourceUrl(imagePath(departments.name)),"toolTipe":toolTipData(departments.name)};
                    depart_scope.push(obj);
                      if(departments.openingTimes){
                        tabservice.push(departments.serviceName);
                        tabData.push(departments.openingTimes);
                        var obj = {name:departments.serviceName.toLowerCase(),"openingTime":departments.openingTimes,"displayname":departments.serviceName};
                        storesOpeningTimes.push(obj);
                      }
                });


                $scope.findAndReplace = function(arrayVal,searchVal,replaceVal){
                    var key = 0;
                    angular.forEach(arrayVal, function(values,keys) {
                      if(values["dates"]!= "undefined" && values["dates"] == searchVal){
                        delete arrayVal[key];
                        arrayVal[key] = replaceVal;
                      }
                      key++;
                    });
                };

                var weekdays = new Array(7);
                weekdays['sun'] = "Sunday";
                weekdays['mon'] = "Monday";
                weekdays['tue'] = "Tuesday";
                weekdays['wed'] = "Wednesday";
                weekdays['thu'] = "Thursday";
                weekdays['fri'] = "Friday";
                weekdays['sat'] = "Saturday";

                var storeOpeningTimes_scope = $scope.storeOpeningTimes;
                $scope.selectedStore = selectedStore_scope;
                $scope.services = services_scope;
                $scope.department = depart_scope;
                var tabContent = [];
                var i = 0;
                angular.forEach(storesOpeningTimes, function(tabNval,tabNkey) {
                  var openAndClose = new Array();
                  var holidayclosed = new Array();
                  var opened = false;
                  var curentDate = new Date(); // please change the date manually here formate should be "2016-12-25"
                  var currentDay = curentDate.getDay();
                  var currentDayName = $filter('date')(curentDate, 'EEE');
                  var firstday = (currentDay == 0) ? curentDate.setDate(curentDate.getDate() - currentDay-6) : curentDate.setDate(curentDate.getDate() - currentDay+1);
                  var j= 0;
                  angular.forEach(tabNval.openingTime, function(val,tabkey) {
                      if(j==0){
                        var startDate = $filter('date')(firstday, 'dd');
                      }
                      else{
                        firstday = curentDate.setDate(curentDate.getDate()+1);
                        var startDate = $filter('date')(firstday, 'dd');
                      }

                      var recordDay = $filter('date')(firstday, 'EEE');
                      var highLightDay = false;
                      if(currentDayName==recordDay){
                        highLightDay = true;
                      }
                      var openingTimeArr = val.open.split(':');
                      var openingTime = (val.open!="00:00:00") ? getTimeFormateFilter(val.open) :"";
                      var closeTimeArr = val.close.split(':');
                      var closeTime = (val.close!="00:00:00") ? getTimeFormateFilter(val.close) :getTimeFormateFilter(val.close);
                      var time = (openingTime!="" && closeTime!="") ? openingTime.toLowerCase() +" - "+closeTime.toLowerCase() : "Closed";
                      var dayobj={"dayName":weekdays[tabkey],"openTiming":time,"dates":startDate,"highLightDay":highLightDay,"serviceName":tabNval.displayname}
                      openAndClose.push(dayobj);
                      j++;
                  });
                /*************** Get special opening timing start here **************/
                  if(storeSpecialTiming.length > 0 ){
                      angular.forEach(storeSpecialTiming, function(val,key) {
                          if(tabNval.name==val.name){ // checking the special opening timing against the service name
                              var specialDate = new Date();
                              var specailDay = specialDate.getDay();
                              var specialStartDay = (specailDay == 0) ? specialDate.setDate(specialDate.getDate()) : specialDate.setDate(specialDate.getDate() - specailDay + 7);
                              var specialWeekEnd = specialDate.setDate(specialDate.getDate() +7);
                              angular.forEach(val.specialOpeningTimes, function(specval,specKey) {
                              var date1 = new Date(); // please change the date manually here formate should be "2016-12-25"
                              var startWeek = $filter('date')(firstday, 'yyyy-MM-dd');
                              var nextDate = new Date(specval.date);
                              nextDate.setDate(nextDate.getDate());
                              var beforeDate  = $filter('date')(nextDate, 'yyyy-MM-dd');
                              var displayDay  = $filter('date')(nextDate, 'EEEE');
                              var currentDays  = $filter('date')(nextDate, 'dd');
                              var dayindex = nextDate.getDay();
                              var currentIndex = date1.getDay();
                              var date2 = new Date(beforeDate);
                              var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
                              var firstday = date1.setDate(date1.getDate() - date1.getDay()+1);
                              var recordDay = $filter('date')(nextDate, 'EEE');
                              var highLightDay = false;
                                if(currentDayName==recordDay){
                                  highLightDay = true;
                                }
                                if(diffDays <= 14 && diffDays >= -7){
                                  if(specval.open){

                                  var open = getTimeFormateFilter(specval.open);
                                  var colse = (specval.close!="00:00:00") ? getTimeFormateFilter(specval.close) :getTimeFormateFilter(specval.close);
                                  var time = (open!="" && colse!="") ? open.toLowerCase() +" - "+colse.toLowerCase() : "Closed";
                                  var holiday = {"dayName":specval.label,"specialopenTiming":time};
                                  var dayobj={"dayName":specval.label,"openTiming":time,"dates":currentDays,"highLightDay":highLightDay,"serviceName":tabNval.displayname};
                                  if(nextDate > specialStartDay && nextDate <= specialWeekEnd){
                                    holidayclosed.push(holiday);
                                  }
                                    $scope.findAndReplace(openAndClose,currentDays,dayobj,tabNval.displayname);
                                  }
                                  else{
                                    var holiday = {"dayName":specval.label,"specialopenTiming":"Closed"};
                                    var dayobj={"dayName":specval.label,"openTiming":"Closed","dates":currentDays,"highLightDay":highLightDay,"serviceName":tabNval.displayname}
                                  if(nextDate > specialStartDay && nextDate <= specialWeekEnd){
                                      holidayclosed.push(holiday);
                                    }
                                    $scope.findAndReplace(openAndClose,currentDays,dayobj,tabNval.displayname);

                                  }
                                }

                              });
                          }
                      });
                  }
                  //console.log(JSON.stringify(openAndClose));
                  /*************** Get special opening timing end here **************/
                  var obj = {"id":i,"serviceName":tabNval.displayname,"openAndClose":openAndClose,'open': opened,"holidays":holidayclosed}
                  tabContent.push(obj);
                  i++;
              });
              $scope.storeOpeningTimes = storeOpeningTimes_scope;
              $scope.tabservice = tabservice;
              $scope.tabContent = tabContent;
          /******************* store high light start here ****************/
              storeHighLighted.get(function(data){
                var storeHightLights = new Array();
                var sevices = data.services;
                sevices = orderByFilter(sevices,"priority",false);
                var hightIncrement = 1;
                  angular.forEach(sevices, function(val,key) {
                    if ( $.inArray(val.name, servicesArr) > -1) {
                        if(hightIncrement<=4){
                          //  var obj = {"priority":val.priority,"img":url+"assets/images/store-highlights/"+val.name+".jpg","serviceName":val.serviceName};
                            var obj = {"priority":val.priority,"img":"./images/store-highlights/"+val.name+".jpg","serviceName":val.serviceName};
                            storeHightLights.push(obj);
                        }
                        hightIncrement++;
                    }
                  });
                  //console.log(url);
                  $scope.storeHightLight = storeHightLights;
                });

          /******************* store high light end here ****************/

          /****************** Store deliver in local area start here **********************/
              localDeliverData.getLocalDeliverFlag($routeParams.storeId).then(function(data) {
                  $scope.localDeliverFlag = data;
              });

         /****************** Store deliver in local area end here **********************/
          $scope.progressbar.complete();
      },
      function (error) {
          //console.log("error " + error);

      });

      var storeImg = angular.element( document.querySelector('img#storeImage'));

      objectFitImages(storeImg,
      {
        watchMQ: true
      });



    });
});
