define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:getTimeFormate
   * @function
   * @description
   * # getTimeFormate
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.GetTimeFormate', [])
  	.filter('getTimeFormate', function ($filter) {
      return function(timeArr){ 
			var storeTimeArr = timeArr.split(':');
		    var d = new Date();
		    d.setHours(storeTimeArr[0], storeTimeArr[1], storeTimeArr[2]);
		    var openT = $filter('date')(d, 'h:mma');
		    return openT;
	  };
  	});
});
