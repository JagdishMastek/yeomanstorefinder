define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:getStoreTime
   * @function
   * @description
   * # getStoreTime
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.GetStoreTime', [])
  	.filter('getStoreTime', function ($filter) {
      return function(timeArr,days){
			//console.log(JSON.stringify(timeArr + " -- " + days));
			//var returnTime = "00:00:00##00:00:00";
			var returnTime = "";
			
            angular.forEach(timeArr, function(val,key) {
		        if(key == days.toLowerCase()){		        	
		           returnTime = val.open+"##"+val.close;

		        }
   			 });
			return returnTime;
	  };
  	});
});
