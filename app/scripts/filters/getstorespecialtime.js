define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:getStoreSpecialTime
   * @function
   * @description
   * # getStoreSpecialTime
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.GetStoreSpecialTime', [])
  	.filter('getStoreSpecialTime', function () {
        return function(timeArr,specialdays){			
			var returnTime = "";			  
            angular.forEach(timeArr, function(val,key) {
				if(val.name=="supermarket")				 
                 angular.forEach(val.specialOpeningTimes, function(vals) {
					if(specialdays == vals.date){
						if(vals.open){
							var colse = (vals.close!="00:00:00") ? vals.close :"24:00:00";
		           			returnTime = vals.open+"##"+colse;
		           		}
		           		else{
		           			returnTime = "close##close";
		           		}

		        	}
				 });
		        
   			 });
			return returnTime;
		};
  	});
});
