define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:displayCity
   * @function
   * @description
   * # displayCity
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.DisplayCity', [])
  	.filter('displayCity', function (unsafeFilter) {
      return function (val) {
      	var rest = val.substring(0, val.lastIndexOf(",") + 1);
        var last = val.substring(val.lastIndexOf(",") + 1, val.length);
        var cityName = rest+' <span>'+last+'</span>';
        return unsafeFilter(cityName);   
      };
  	});
});
