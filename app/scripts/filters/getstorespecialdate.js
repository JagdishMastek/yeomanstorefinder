define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:getStoreSpecialDate
   * @function
   * @description
   * # getStoreSpecialDate
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.GetStoreSpecialDate', [])
  	.filter('getStoreSpecialDate', function () {
      return function(timeArr,specialdays){			
			var returnDate = "";			
            angular.forEach(timeArr, function(val,key) {
				if(val.name=="supermarket")
          angular.forEach(val.specialOpeningTimes, function(vals,keys) {				 	
					  if(specialdays == vals.date){
		          		returnDate = vals.date;
		        }
				 });
		        
   		});
			return returnDate;
	  };
  	});
});
