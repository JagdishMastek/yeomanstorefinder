define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:openingClosing
   * @function
   * @description
   * # openingClosing
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.OpeningClosing', [])
  	.filter('openingClosing', function ($filter,getTimeFormateFilter,getStoreTimeFilter,unsafeFilter,getStoreSpecialTimeFilter,getStoreSpecialDateFilter) {
      return function(items,page,specialTime){ 
			//console.log(JSON.stringify(specialTime));
			    var openCloseFlag = "";
			    var HHmmss = $filter('date')(new Date(), 'HH:mm:ss');
			    //var HHmmss = "20:15:15";
			    var today  = $filter('date')(new Date(), 'EEE');    
			    var sepcialToday  = $filter('date')(new Date(), 'yyyy-MM-dd');   
			    //console.log("today==> "+HHmmss);
			    var nextDate = new Date();
			    nextDate.setDate(nextDate.getDate() + 1);
			    var tommrow  = $filter('date')(nextDate, 'EEE'); 
			    var tommrowSpecial = $filter('date')(nextDate, 'yyyy-MM-dd'); 
			    var storeTimeingArr = getStoreTimeFilter(items,today);
              	var tommrowStoreArr = getStoreTimeFilter(items,tommrow);
              	if(specialTime!="undefined"){
                      
              		var specialTimeArr = getStoreSpecialTimeFilter(specialTime,sepcialToday);
              		var specialTimeTommArr = getStoreSpecialTimeFilter(specialTime,tommrowSpecial);
              		var specialTodayDate = getStoreSpecialDateFilter(specialTime,sepcialToday);
              		var specialTommDate = getStoreSpecialDateFilter(specialTime,tommrowSpecial);
              		if(sepcialToday==specialTodayDate){
              			
              			storeTimeingArr = specialTimeArr;
              			
              		}
              		//console.log(JSON.stringify(specialTimeArr));
              		if(tommrowSpecial==specialTommDate){
              			//console.log(JSON.stringify("specialTimeTommArr"+specialTimeTommArr));
              			tommrowStoreArr = specialTimeTommArr;
              		}
              	}
              	
              	if(storeTimeingArr != "" && storeTimeingArr != null) {
					var timingArr = storeTimeingArr.split('##');
		            var tommrowtimingArr = tommrowStoreArr.split('##');
		            //console.log(JSON.stringify(tommrowtimingArr));
		            var opeingTiming =timingArr[0];
		            var closedTiming = timingArr[1];   
		            var tommrowopeingTiming =tommrowtimingArr[0];
		            var tommrowclosedTiming = tommrowtimingArr[1];  
		            if(opeingTiming!="close"){  
		              	var storeOpenTime = getTimeFormateFilter(opeingTiming);   
		              	var storeCloseTime =  getTimeFormateFilter(closedTiming);   	
		              	
	              	}
	              	else{
	              		storeOpenTime = "close";
	              		
	              	}

	              	if(tommrowopeingTiming!="close" && tommrowopeingTiming!="00:00:00"){  
		              	
		              	var tommrowstoreOpenTime = getTimeFormateFilter(tommrowopeingTiming);   
		              	var tommrowstoreCloseTime =  getTimeFormateFilter(tommrowclosedTiming);  
	              	}
	              	else{	              		
	              		tommrowstoreOpenTime = "close";
	              	}
	              	//console.log(JSON.stringify("storeOpenTime "+tommrowstoreCloseTime));
	              	if(page == "listing"){
						if ((HHmmss < opeingTiming)) {
							if(storeOpenTime == "close"){
							 	openCloseFlag = '<span class="hours-red">Store closed today </span><span class="hours-black">Open Tomorrow: '+tommrowstoreOpenTime.toLowerCase()+' - '+tommrowstoreCloseTime.toLowerCase()+'</span>';
							 }
							 else{
								openCloseFlag = '<span class="hours-red">Store closed now</span><span class="hours-black">Open Today: '+storeOpenTime.toLowerCase()+' - '+ storeCloseTime.toLowerCase()+'</span>';
							}
						}
						else if ((HHmmss >= opeingTiming) && (HHmmss <= closedTiming)) {               
							openCloseFlag = '<span class="hours-black">Open Today: '+storeOpenTime.toLowerCase()+' - '+ storeCloseTime.toLowerCase()+'</span>';
						}
						else{
							if(tommrowstoreOpenTime == "close"){
							 	openCloseFlag = '<span class="hours-red">Store closed now</span><span class="hours-red">Store closed tomorrow </span>';
							 }
							 else {
								openCloseFlag ='<span class="hours-red">Store closed now</span><span class="hours-black">Open Tomorrow: '+tommrowstoreOpenTime.toLowerCase()+' - '+tommrowstoreCloseTime.toLowerCase()+'</span>';
							}
						} 
					}
					else{
					if ((HHmmss < opeingTiming)) {
							if(storeOpenTime == "close"){
							 	openCloseFlag = '<span class="hours-red">Store closed today </span><span class="hours-black">Open Tomorrow: '+tommrowstoreOpenTime.toLowerCase()+' - '+tommrowstoreCloseTime.toLowerCase()+'</span>';
							 }
							 else{
								openCloseFlag = '<span class="hours-red">Store closed now</span><span class="hours-black">Open Today: '+storeOpenTime.toLowerCase()+' - '+ storeCloseTime.toLowerCase()+'</span>';
							}
						}
						else if ((HHmmss >= opeingTiming) && (HHmmss <= closedTiming)) {               
							openCloseFlag = '<span class="hours-black">Open Today: '+storeOpenTime.toLowerCase()+' - '+ storeCloseTime.toLowerCase()+'</span>';
						}
						else{
							 if(tommrowstoreOpenTime == "close"){
							 	openCloseFlag = '<span class="hours-red">Store closed now</span><span class="hours-red">Store closed tomorrow </span>';
							 }
							 else{
								openCloseFlag ='<span class="hours-red">Store closed now</span><span class="hours-black">Open Tomorrow: '+tommrowstoreOpenTime.toLowerCase()+' - '+tommrowstoreCloseTime.toLowerCase()+'</span>';
							}
						}
					}
				}
				
			return unsafeFilter(openCloseFlag);		
		};
  	});
});
