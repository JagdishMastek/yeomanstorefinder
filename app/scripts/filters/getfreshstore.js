define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:getFreshStore
   * @function
   * @description
   * # getFreshStore
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.GetFreshStore', [])
  	.filter('getFreshStore', function (freshStoreConfig) {  
      return function (storeId) {
          var freshFlag = false;
	        var  freshFilter = freshStoreConfig; 	        
            if(jQuery.inArray(storeId, freshFilter) !== -1){
                freshFlag = true;
            }
      return freshFlag;
      }; 
  	});
});
