define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc filter
   * @name storesApp.filter:unsafe
   * @function
   * @description
   * # unsafe
   * Filter in the storesApp.
   */
  angular.module('storesApp.filters.Unsafe', [])
  	.filter('unsafe', function ($sce) {
      return function(val) {
        return $sce.trustAsHtml(val);
        };
  	});
});
