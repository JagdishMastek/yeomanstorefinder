



/*jshint unused: vars */
define(['angular', 'controllers/storesearch', 'controllers/searchresult', 'services/dataservice', 'services/localstorageservice', 'filters/displaycity', 'filters/unsafe', 'services/store', 'services/storeall', 'filters/getfreshstore', 'filters/getstoretime', 'filters/gettimeformate', 'filters/openingclosing', 'filters/getstorespecialdate', 'filters/getstorespecialtime', 'services/config', 'controllers/storedetails', 'services/geolocation', 'services/imagepath', 'services/tooltipdata', 'services/storehighlighted', 'directives/customcollapse', 'directives/backbutton', 'directives/bingmap', 'controllers/route', 'controllers/storeroute', 'directives/bingroute', 'directives/bingmapstore', 'services/analytics', 'services/bingfactory', 'directives/onerrorsrc', 'directives/timeyear', 'services/localdeliverdata', 'services/fakestorage'] /*deps*/ , function(angular, StoreSearchCtrl, SearchResultCtrl, StoreConfirmCtrl, LocalStorageServiceService, DisplayCityFilter, UnsafeFilter, StoreFactory, StoreAllFactory, GetFreshStoreFilter, GetStoreTimeFilter, GetTimeFormateFilter, OpeningClosingFilter, GetStoreSpecialDateFilter, GetStoreSpecialTimeFilter, ConfigConstant, StoreDetailsCtrl, GeolocationFactory, ImagePathValue, ToolTipDataValue, StoreHighLightedFactory, CustomCollapseDirective, BackButtonDirective, BingMapDirective, RouteCtrl, StoreRouteCtrl, BingRouteDirective, BingMapStoreDirective, AnalyticsService, AngularSeoService, BingFactoryFactory, OnErrorSrcDirective, TimeYearDirective, LocalDeliverDataFactory, FakeStorageFactory) /*invoke*/ {
    'use strict';

    /**
     * @ngdoc overview
     * @name storesApp
     * @description
     * # storesApp
     *
     * Main module of the application.
     */
    return angular
        .module('storesApp', ['storesApp.controllers.StoreSearchCtrl',
            'storesApp.controllers.SearchResultCtrl',
            'storesApp.services.LocalStorageService',
            'storesApp.filters.DisplayCity',
            'storesApp.filters.Unsafe',
            'storesApp.services.Store',
            'storesApp.services.StoreAll',
            'storesApp.filters.GetFreshStore',
            'storesApp.filters.GetStoreTime',
            'storesApp.filters.GetTimeFormate',
            'storesApp.filters.OpeningClosing',
            'storesApp.filters.GetStoreSpecialDate',
            'storesApp.filters.GetStoreSpecialTime',
            'storesApp.services.Config',
            'storesApp.controllers.StoreDetailsCtrl',
            'storesApp.services.Geolocation',
            'storesApp.services.ImagePath',
            'storesApp.services.ToolTipData',
            'storesApp.services.StoreHighLighted',
            'storesApp.directives.CustomCollapse',
            'storesApp.directives.BackButton',
            'storesApp.directives.BingMap',
            'storesApp.directives.BingMapStore',
            'storesApp.directives.BingRoute',
            'storesApp.directives.TimeYear',
            'storesApp.controllers.RouteCtrl',
            'storesApp.controllers.StoreRouteCtrl',
            'storesApp.services.Analytics',
            'storesApp.services.BingFactory',
            'storesApp.directives.OnErrorSrc',
            
'storesApp.services.LocalDeliverData',
'storesApp.services.FakeStorage',
/*angJSDeps*/
            'ngResource', 'ngSanitize', 'ngRoute', 'ngAnimate', 'ngTouch', 'ngMeta', 'ngProgress', 'asyncFilter','angularytics','seo'
        ])
        .config(function($routeProvider, $locationProvider, $httpProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'views/storesearch.html',
                    controller: 'StoreSearchCtrl',
                    controllerAs: 'storeSearch',
                    label: 'Home'

                })
                .when('/stores/:searchTerm', {
                    templateUrl: 'views/searchresult.html',
                    controller: 'SearchResultCtrl',
                    controllerAs: 'searchResult',
                    label: 'Home'
                })
                .when('/:storeId', {
                    templateUrl: 'views/storedetails.html',
                    controller: 'StoreDetailsCtrl',
                    controllerAs: 'storeDetails',
                    label: 'Home'

                })
                .when('/:storeId/:travelFrom', {
                    templateUrl: 'views/storeroute.html',
                    controller: 'StoreRouteCtrl',
                    controllerAs: 'storeRoute',
                    label: 'Home'
                })
                .otherwise({
                    redirectTo: '/'
                });

                $locationProvider.hashPrefix('!');
                $locationProvider.html5Mode(true);
            // Removed (#!) hashPrefix

            /*if (/^#!/.test(location.hash)) {
                var url = String( document.location.href ).replace( "#!/", "" );   
                location.href = url;
            }*/
            //$locationProvider.hashPrefix('!');

        var interceptor = ['$q', '$injector', '$timeout', '$rootScope', function($q, $injector, $timeout, $rootScope) {
          return {
            response: function(resp) {
              var $http = $injector.get('$http');
              if (!$http.pendingRequests.length) {
                $timeout(function() {
                  if (!$http.pendingRequests.length) {
                    $rootScope.htmlReady();
                  }
                }, 0); // Use .7s as safety interval
              }
              return resp;
            }
          };
        }];

    $httpProvider.interceptors.push(interceptor);

        }).run(['ngMeta', function(ngMeta) {
            ngMeta.init();
        }]).run(function($rootScope, $location) {

            var history = [];

            $rootScope.$on('$routeChangeSuccess', function() {
                history.push($location.$$path);
            });

            $rootScope.back = function() {
                var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
                $location.path(prevUrl);
            };
        }).config(function(AngularyticsProvider) {
    AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
  }).run(function(Angularytics) {
    Angularytics.init();
  });
});