define(['angular'], function(angular) {
    'use strict';

    /**
     * @ngdoc service
     * @name storesApp.bingFactory
     * @description
     * # bingFactory
     * Factory in the storesApp.
     */
    angular.module('storesApp.services.BingFactory', [])
        .factory('bingFactory', function($http, cfg) {
            return {
                get: function(searchTerm) {
                    //console.log("searchTerm "+searchTerm);
                    var searchCountry = "UK";
                    var searchQuery = encodeURIComponent(searchTerm.replace(" ", ""));
                    if (searchQuery.toLowerCase() === "gibraltar" || searchQuery.toLowerCase() === "gx11 1aa" || searchQuery.toLowerCase() === "gx111aa") {
                        searchCountry = "GI";
                    }
                    var url = "https://dev.virtualearth.net/REST/v1/Locations?query=" + encodeURIComponent(searchTerm) + "," + searchCountry + "&output=json&jsonp=JSON_CALLBACK&key=" + cfg.bing_api_key;
                    //console.log(url);
                    return $http.jsonp(url)
                        .success(function(data) {
                            return data;

                        })
                        .error(function(data) {
                            console.log(data);
                        });
                }
            };
        });
});