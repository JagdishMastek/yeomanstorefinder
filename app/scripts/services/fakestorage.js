define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.fakeStorage
   * @description
   * # fakeStorage
   * Factory in the storesApp.
   */
  angular.module('storesApp.services.FakeStorage', [])
    .factory('fakeStorage', function () {
        function FakeStorage() {};
            FakeStorage.prototype.setItem = function (key, value) {
              this[key] = value;
            };
            FakeStorage.prototype.getItem = function (key) {
              return typeof this[key] == 'undefined' ? null : this[key];
            }
            FakeStorage.prototype.removeItem = function (key) {
              this[key] = undefined;
            };
            FakeStorage.prototype.clear = function(){
              for (var key in this) {
                if( this.hasOwnProperty(key) )
                {
                  this.removeItem(key);
                }
              }
            };
            FakeStorage.prototype.key = function(index){
              return Object.keys(this)[index];
            };
            return new FakeStorage();
    });
});
