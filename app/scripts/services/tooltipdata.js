define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.toolTipData
   * @description
   * # toolTipData
   * Value in the storesApp.
   */
  angular.module('storesApp.services.ToolTipData', [])
      .value('toolTipData', function (service) {
        var toolTipData = null;


      if (service === "24HourCash") {
            toolTipData = 'Get cash any time at the convenient ATMs outside our store';
        }

        else if (service === "amazonLockers") {
            toolTipData = 'Amazon Lockers';
        }
            else if (service === "asianClothing") {
            toolTipData = 'Asian Clothing';
        }
          else if (service === "babyChanging") {
            toolTipData = 'Baby Changing';
        }
              else if (service === "byboxLockers") {
            toolTipData = 'By Box Lockers';
        }
        else if (service === "calorGas") {
            toolTipData = 'Calor Gas LPG cylinders can be used for patio heaters, barbeques, caravanning and torches.';
        }

        else if (service === "carpetCleaning") {
            toolTipData = "If your carpets could do with a clean or you're thinking about a new carpet, rent a Rug Doctor first - you'll be amazed at the results!";
        }

        else if (service === "carwash") {
            toolTipData = 'Stop by our petrol station to fill up and enjoy many other services like gas, a car wash and snacks for the road';
        }
        else if (service === "coffeeToGo") {
            toolTipData = 'Coffee To Go';
        }
        else if (service === "coinstarMachines") {
            toolTipData = 'Coinstar Machines';
        }
        else if (service === "deli") {
            toolTipData = 'Deli';
        }

        else if (service === "disabledFacilities") {
            toolTipData = 'We offer lots of services to our shoppers with additional needs, including dedicated parking, wheelchairs, staff assistance, seating and induction loops';
        }

        else if (service === "disabledToilets") {
            toolTipData = 'Disabled Toilets';
        }
        else if (service === "doddleParcels") {
            toolTipData = 'Doddle Parcels';
        }
        else if (service === "dryCleaning") {
            toolTipData = 'Dry Cleaning';
        }
        else if (service === "flowerShop") {
            toolTipData = 'Flower Shop';
        }
        else if (service === "flowerWrapping") {
            toolTipData = 'Flower Wrapping';
        }
        else if (service === "foreignExchange") {
            toolTipData = 'Foreign Exchange';
        }
        else if (service === "freeFrom") {
            toolTipData = 'Free From';
        }
        else if (service === "freeParking") {
    toolTipData = 'Enjoy Free Parking at this store';
        }
        else if (service === "freshToGo") {
    toolTipData = 'Fresh To Go';
        }
        else if (service === "halalMeat") {
    toolTipData = 'Halal Meat';
        }
        else if (service === "halalCounter") {
    toolTipData = 'Halal Counter';
        }
        else if (service === "hotFood") {
    toolTipData = 'Hot Food';
        }
        else if (service === "inpostLockers") {
    toolTipData = 'Inpost Lockers';
        }
        else if (service === "keyCutting") {
    toolTipData = 'Key Cutting';
        }
        else if (service === "lpg") {
            toolTipData = 'We stock LPG autogas, another cost effective and environmentally-friendly fuel alternative to petrol and diesel.';
        }
        else if (service === "makeYourOwnPizza") {
            toolTipData = 'Make Your Own Pizza';
        }
        else if (service === "nationalLottery") {
            toolTipData = 'Get your ticket at the Lottery desk in store';
        }
        else if (service === "ovenFresh") {
            toolTipData = 'Oven Fresh';
        }
        else if (service === "pancakes") {
            toolTipData = 'Pancakes';
        }
        else if (service === "parentAndChild") {
            toolTipData = 'We offer convenient parking for parents with small children, making it easier to manage prams and trolleys';
        }
        else if (service === "partyZone") {
            toolTipData = 'Party Zone';
        }



        else if (service === "bakery") {
            toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
        }

        else if (service === "cafe") {
            toolTipData = 'If you fancy a relaxing bite to eat, our cafe serves freshly prepared meals to your table, from all-day breakfasts to hot puddings with custard as well as freshly ground coffee.';
        }
        else if (service === "pharmacy") {
            toolTipData = 'Get free advice on medicines and healthcare from our pharmacists - no appointment necessary - and why not have your prescriptions dispensed while you shop.';
        }
        else if (service === "butcher") {
            toolTipData = 'Our trained butchers cut and prepare 100% fresh British beef, pork and lamb in-store, offering you a larger selection of cuts than any other supermarket.';
        }
        else if (service === "provisions") {
            toolTipData = 'Ask for a little taste before you buy, and we’ll cut and weigh anything from our range of delicacies, including cooked meat, olives and cheese.';
        }
        else if (service === "nutmeg") {
            toolTipData = 'Nutmeg is a clothing brand full of thoughtful details. Little things that make everything a little easier, a little brighter and a lot more comfortable..';
        }
        else if (service === "ovenFresh") {
            toolTipData = 'Enjoy all your favourite sweet and savoury pies, and comfort food like roast chicken, and ribs – straight from the oven.';
        }
        else if (service === "saladBar") {
            toolTipData = 'Grab one of our pizzas or salads, freshly prepared in store!';
        }
        else if (service === "fishmonger") {
            toolTipData = 'Our trained fishmongers are available all day to prepare any of our fish, plus advise on the best buys and preparation ideas.';
        }
        else if (service === "theCakeShop") {
            toolTipData = 'Many of our tempting cakes and desserts – pastries, tarts, doughnuts and gateaux - are prepared by hand in store throughout the day.';
        }
        else if (service === "recycling") {
            toolTipData = 'Recycling.';
        }


        else if (service === "photoPrinting") {
            toolTipData = 'We offer quality digital photo printing in-store, with 1 hour services available.';
        }
        else if(service === "gardenCentre") {
            toolTipData = "Plan your perfect garden by visiting the Garden Centre when you're next in store";
        }

        return toolTipData;
    });
});
