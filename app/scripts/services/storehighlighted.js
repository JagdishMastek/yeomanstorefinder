define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.storeHighLighted
   * @description
   * # storeHighLighted
   * Factory in the storesApp.
   */
  angular.module('storesApp.services.StoreHighLighted', [])
    .factory('storeHighLighted', function ($resource) {
      return $resource('./data/ServiceHighlight.json');
    });
});
