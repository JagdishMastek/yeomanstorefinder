define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.storeAll
   * @description
   * # storeAll
   * Factory in the storesApp.
   */
  angular.module('storesApp.services.StoreAll', [])
    .factory('storeAll', function ($q, $http, store) {
        return {
    getNestedData: function(data) {
      
      var promises = [];
      var deferredCombinedItems = $q.defer();
      var combinedItems = [];
      
      //return $http.get('https://uat-api.morrisons.com/location/v2/'+data.dest+'?apikey='+data.apikey+'&distance='+data.distance+'&lat='+data.lat+'&limit='+data.limit+'&lon='+data.lon+'&offset='+data.offset+'&storeformat='+data.storeformat).then(function(res) {
        return store.getStoreAll({dest:data.dest,apikey:data.apikey ,distance:data.distance,lat:data.lat,limit:data.limit,lon:data.lon,offset:data.offset,storeformat:data.storeformat}).$promise.then(function(res) {
            //console.log(JSON.stringify(res));
            angular.forEach(res.stores, function(list,key) {
              var deferredItemList = $q.defer();
              var storeIds = list.name;
              
              //$http.get('https://uat-api.morrisons.com/location/v2/'+data.dest+'/'+storeId+'/specialopeningtimes?apikey='+data.apikey).then(function(res) {
              store.getStoreOpening({dest:data.dest,storeId:storeIds,openingtime:'specialopeningtimes',apikey:data.apikey}).$promise.then(function(res) { 
              var returnedItems = res;
              combinedItems[key] = {"stores":list,"specialtime":returnedItems};
              //list = list.concat(returnedItems);
              
                deferredItemList.resolve();
                //console.log(JSON.stringify(list));
              });
              
              promises.push(deferredItemList.promise);
            });
            
            $q.all(promises).then( function() {
              deferredCombinedItems.resolve(combinedItems);
            });
            
            return deferredCombinedItems.promise;
            
          });

          }
      };
    });
});
