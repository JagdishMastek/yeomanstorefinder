define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.DataService
   * @description
   * # DataService
   * Service in the storesApp.
   */
  angular.module('storesApp.services.DataService', [])
	.service('DataService', function () {
      var userData = null;

      var setData = function(data) {
          userData = data;
      };

      var getData = function(){
          return userData;
      };

      return {
        setData: setData,
        getData: getData
      };
	});
});
