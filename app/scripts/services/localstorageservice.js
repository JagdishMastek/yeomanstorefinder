define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.LocalStorageService
   * @description
   * # LocalStorageService
   * Service in the storesApp.
   */
  angular.module('storesApp.services.LocalStorageService', [])
  .service('LocalStorageService', function ($window,fakeStorage) {
      /*this.getItem = function(key){
        return $window.sessionStorage.getItem(key);
      };
      this.setItem = function(key,value){
          return $window.sessionStorage.setItem(key, value);
      };
      this.removeItem = function(key){
          return $window.sessionStorage.removeItem(key);
      };
      this.clearAll = function() {
          return $window.sessionStorage.clear();
      };*/
      function isStorageSupported(storageName)
    {
      var testKey = 'test',
        storage = $window[storageName];
      try
      {
        storage.setItem(testKey, '1');
        storage.removeItem(testKey);
        return true;
      }
      catch (error)
      {
        return false;
      }
    }

        var storage = isStorageSupported('localStorage') ? $window.localStorage : fakeStorage;
        //console.log( JSON.stringify(storage));
    return {
      setItem: function(key, value) {
        storage.setItem(key, value);
      },
      getItem: function(key, defaultValue) {
        return storage.getItem(key) || defaultValue;
      },
      setObject: function(key, value) {
        storage.setItem(key, JSON.stringify(value));
      },
      getObject: function(key) {
        return JSON.parse(storage.getItem(key) || '{}');
      },
      removeItem: function(key){
        storage.removeItem(key);
      },
      clearAll: function() {
        storage.clear();
      },
      key: function(index){
        storage.key(index);
      }
    }

  });
});
