define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.store
   * @description
   * # store
   * Factory in the storesApp.
   */
  angular.module('storesApp.services.Store', ['ngResource'])
    .factory('store', function ($resource) {
          return $resource("https://uat-api.morrisons.com/location/v2/:dest/:storeId/:openingtime", {},{
            'getStoreAll': {method: 'GET', isArray: false } ,
            'getStore': {method: 'GET', isArray: false }, 
            'getStoreOpening': {method: 'GET', isArray: true } 
          });
    });
});
