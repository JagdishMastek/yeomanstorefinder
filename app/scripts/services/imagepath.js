define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.imagePath
   * @description
   * # imagePath
   * Value in the storesApp.
   */
  angular.module('storesApp.services.ImagePath', [])
    .value('imagePath', function (service) {
      var images = null;
      if (service === "nationalLottery") {
          images = './images/store-services/Lottery.svg';
      }
      else if (service === "cafe") {
          images = './images/store-services/Cafe.svg';
      }
      else if (service === "pharmacy") {
          images = './images/store-services/Pharmacy.svg';
      }
      else if (service === "bakery") {
          images = './images/store-services/Bakery.svg';
      }
      else if (service === "butcher") {
          images = './images/store-services/Butchery.svg';
      }
      else if (service === "provisions") {
          images = './images/store-services/GardenCentre.svg';
      }
      else if (service === "nutmeg") {
          images = './images/store-services/NutmegClothing.svg';
      }
      else if (service === "ovenFresh") {
          images = './images/store-services/OvenFresh.svg';
      }
      else if (service === "saladBar") {
          images = './images/store-services/Deli.svg';
      }
      else if (service === "fishmonger") {
          images = './images/store-services/Fishmonger.svg';
      }
      else if (service === "theCakeShop") {
          images = './images/store-services/CakeShop.svg';
      }
      else if (service === "freeParking") {
          images = './images/store-services/Parking.svg';
      }
      else if (service === "disabledFacilities") {
          images = './images/store-services/DisabledAccess.svg';
      }
      else if (service === "recycling") {
          images = './images/store-services/Recycling.svg';
      }
      else if (service === "parentAndChild") {
          images = './images/store-services/ParentChildParking.svg';
      }
      else if (service === "24HourCash") {
          images = './images/store-services/ATM.svg';
      }
      else if (service === "calorGas") {
          images = './images/store-services/CalorGas.svg';
      }
      else if (service === "carpetCleaning") {
          images = './images/store-services/RugDoctor.svg';
      }
      else if (service === "carwash") {
          //      images = 'assets/image/icon_petrol.gif';
          images = './images/store-services/CarWash.svg';
      }
      else if (service === "lpg") {
          images = './images/store-services/GardenCentre.svg';
      }
      else if (service === "dryCleaning") {
          //      images = 'assets/image/icon_cakeshop.gif';
          images = './images/store-services/DryCleaning.svg';
      }
      else if (service === "photoPrinting") {
          images = './images/store-services/PhotoProcessing.svg';
      }
      //    else if(service==="photoPrinting"){
      else if (service === "matchAndMore") {
          images = './images/store-services/GardenCentre.svg';
      }
      else if (service === "gardenCentre") {
          images = "./images/store-services/GardenCentre.svg";
      }
      else if (service === "pfs") {
          
          images = "./images/store-services/Petrol.svg";
      }
      //console.log(service+" IMAGE "+images);
      return images;
  });
});
