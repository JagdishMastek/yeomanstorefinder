define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.geolocation
   * @description
   * # geolocation
   * Factory in the storesApp.
   */
  angular.module('storesApp.services.Geolocation', [])
    .factory('geolocation', function ($q) {
        var getLocation = function() {
            var defer = $q.defer();
            // If supported and have permission for location...
            if (navigator.geolocation) {
                // 
                navigator.geolocation.getCurrentPosition(function(position){
                    var result = {latitude : position.coords.latitude , longitude : position.coords.longitude};
                      // Adding randomization since we are all in the same location...
                      result.latitude += (Math.random() >0.5? -Math.random()/100 : Math.random()/100  );
                      result.longitude += (Math.random() >0.5? -Math.random()/100 : Math.random()/100  );
                      defer.resolve(result);                    
                      }, function(error){                   
                          defer.reject({message: error.message, code:error.code});
                      },{enableHighAccuracy:true,maximumAge:Infinity, timeout:60000}
                    );
            }
            else {
                defer.reject({error: 'Geolocation not supported'});
            }
            return defer.promise;
        };
        var service = {
            getLocation : getLocation
        };
        return service;
    });
});
