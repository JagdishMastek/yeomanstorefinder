define(['angular'], function (angular) {
  'use strict';
  var _gaq = [];
  /**
   * @ngdoc service
   * @name storesApp.Analytics
   * @description
   * # Analytics
   * Service in the storesApp.
   */
  angular.module('storesApp.services.Analytics', []).run(['$http', function($http) {
  
	_gaq.push(['_setAccount', 'UA-19890257-2']);
	_gaq.push(['_trackPageview']);

	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);

}]).service('Analytics', function ($rootScope, $window, $location, $routeParams) {
       

      var track = function() {
        //console.log(" i m in track");
        var path = convertPathToQueryString($location.path(), $routeParams);
       _gaq.push(['_trackPageview', path]);
      };
      
      var convertPathToQueryString = function(path, $routeParams) {
        for (var key in $routeParams) {
          var queryParam = '/' + $routeParams[key];
          path = path.replace(queryParam, '');
        }

        var querystring = decodeURIComponent($.param($routeParams));

        if (querystring === '') { return path ;}

        return path + "?" + querystring;
      };

       $rootScope.$on('$viewContentLoaded', track);
	});
});
