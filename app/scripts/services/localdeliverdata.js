define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name storesApp.localDeliverData
   * @description
   * # localDeliverData
   * Factory in the storesApp.
   */
  angular.module('storesApp.services.LocalDeliverData', [])
    .factory('localDeliverData', function ($resource) {
     

      // Public API here
      
        var getLocalDeliverFlag = function (storeId) {
          return $resource('./data/DeliverLocalArea.json', {}).get().$promise.then(function(data) {            
                  var stores = data.stores; 
                  var localdeliverFlag = false;
                  angular.forEach(stores, function(val,key) {
                      if (val.id == storeId && val.deliverLocalArea == "True") {
                            localdeliverFlag  = true;   
                      }                      
                  });                  
                  return localdeliverFlag;
            });
        };

         return {
          getLocalDeliverFlag: getLocalDeliverFlag
        };

     
    });
});
