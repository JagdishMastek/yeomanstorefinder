/*jshint unused: vars */
require.config({
    paths: {
        angular: '../../bower_components/angular/angular',
        'angular-animate': '../../bower_components/angular-animate/angular-animate',
        'angular-mocks': '../../bower_components/angular-mocks/angular-mocks',
        'angular-resource': '../../bower_components/angular-resource/angular-resource',
        'angular-route': '../../bower_components/angular-route/angular-route',
        'angular-sanitize': '../../bower_components/angular-sanitize/angular-sanitize',
        'angular-touch': '../../bower_components/angular-touch/angular-touch',
        ngMeta: '../../bower_components/ngMeta/dist/ngMeta',
        'angular-cookies': '../../bower_components/angular-cookies/angular-cookies',
        ngprogress: '../../bower_components/ngprogress/build/ngprogress.min',
        'angular1-async-filter': '../../bower_components/angular1-async-filter/async-filter',
        jquery: '../../bower_components/jquery/dist/jquery',
        bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap',
        'angular-seo': '../../bower_components/angular-seo/angular-seo',
        angularytics: '../../bower_components/angularytics/dist/angularytics.min',
        objectFitPolyfill: '../../bower_components/objectFitPolyfill/dist/objectFitPolyfill.min',
        'angular-bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls'
    },
    priority: [
        'angular'
    ],
    shim: {
        angular: {
            exports: 'angular'
        },
        'angular-route': [
            'angular'
        ],
        'angular-sanitize': [
            'angular'
        ],
        'angular-resource': [
            'angular'
        ],
        'angular-animate': [
            'angular'
        ],
        'angular-touch': [
            'angular'
        ],
        ngprogress: [
            'angular'
        ],
        'angular1-async-filter': [
            'angular'
        ],
        ngMeta: [
            'angular'
        ],
        angularytics: [
            'angular'
        ],
        bootstrap: [
            'jquery'
        ],
        'angular-seo': [
            'angular'
        ],
        'angular-mocks': {
            deps: [
                'angular'
            ],
            exports: 'angular.mock'
        }
    },
    packages: [

    ]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

require([
    'angular',
    'jquery',
    'app',
    'angular-route',
    'angular-sanitize',
    'angular-resource',
    'angular-animate',
    'angular-touch',
    'bootstrap',
    'ngMeta',
    'ngprogress',
    'angular1-async-filter',
    'angularytics',
    'angular-seo',
], function(angular, jquery, app, ngRoutes, ngSanitize, ngResource, ngAnimate, ngTouch, ngMeta, ngProgress, asyncFilter, angularytics,seo) {
    'use strict';
    /* jshint ignore:start */
    var $html = angular.element(document.getElementsByTagName('html')[0]);
    /* jshint ignore:end */
    angular.element(document).ready(function() {

        var initInjector = angular.injector(['ng']);
        var $http = initInjector.get('$http');
        $http.get('./data/FreshLook.json').then(
            function(response) {
                angular.bootstrap(document, ['storesApp']);
                var myApp = angular.module(app.name);
                var freshArr = [];
                angular.forEach(response.data, function(value, keys) {
                    angular.forEach(value, function(val, key) {
                        if (val.freshLookStatus === 3) {
                            freshArr.push(val.id);
                        }
                    });
                });
                //console.log(freshArr);
                myApp.constant('freshStoreConfig', freshArr);
                angular.resumeBootstrap([app.name]);
            }
        );

    });
});