define(['angular'], function(angular) {
    'use strict';

    /**
     * @ngdoc directive
     * @name storesApp.directive:timeYear
     * @description
     * # timeYear
     */
    angular.module('storesApp.directives.TimeYear', [])
        .directive('timeYear', ['$filter', function($filter) {
            return {
                restrict: 'A',
                replace: true,
                link: function(scope, element) {
                    element.text($filter('date')(new Date(), 'yyyy'));
                }
            };
        }]);
});