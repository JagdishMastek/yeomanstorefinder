define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc directive
   * @name storesApp.directive:backButton
   * @description
   * # backButton
   */
  angular.module('storesApp.directives.BackButton', [])
    .directive('backButton', function ($window) {
      return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.bind('click', function () {
                    //console.log("i m back");
                    $window.history.back();
                    scope.$apply();
                });
            }
        };
    });
});
