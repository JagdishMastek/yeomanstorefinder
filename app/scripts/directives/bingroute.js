define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc directive
   * @name storesApp.directive:bingRoute
   * @description
   * # bingRoute
   */
  angular.module('storesApp.directives.BingRoute', [])
    .directive('bingRoute', function ($window) {
       return {  
       restrict: 'EA',
	     replace: true,
       scope:{
          bingRoute: '='
       },
       
        template: '<div class="tab-content"><div role="tabpanel" class="tab-pane active col-xs-12 col-sm-4" id="directions"><div id="itineraryDiv" style="position:relative;"></div></div>' +  
                  '<div role="tabpanel" class="tab-pane col-xs-12 col-sm-8 map" id="directions-map"><div class="col-xs-12" id="map-outer"><div id="mapDiv"></div></div></div></div>',  
        link: function (scope, element, attrs) {  
              function getMap(){
                
                
                  scope.$watchCollection("bingRoute", function(newValue) {
                    if(angular.isDefined(newValue) && newValue !== null) {
                      
                      scope.mapOption = newValue;
                      //{"latitudeto":53.7788696289062,"longitudeto":-1.5286500453949,"latitudefrom":"53.794193267822266","longitudefrom":"-1.7597182989120483"}
                         
                          scope.latitudeTo = newValue.latitudeto;
                          scope.longitudeTo = newValue.longitudeto;
                          scope.latitudeFrom = newValue.latitudefrom;
                          scope.longitudeFrom = newValue.longitudefrom;
                      //console.log(newValue.latitudeto);
                          var mapOptions = {
                            credentials: 'Ag00iheqslugEVbwF43XtJCoY0tAUdcyXd8XS3oA4l389lpCxFj2vYXknu_B_0ua', 
                            //zoom : 8, 
                            showDashboard: true, 
                            showMapTypeSelector:true, 
                            enableHighDpi: true,
                            showCopyright:false,
                            enableSearchLogo: false,
                            enableClickableLogo: false
                          };
                          
                          scope.map = new Microsoft.Maps.Map(document.getElementById("mapDiv"), mapOptions); 
                          Microsoft.Maps.loadModule('Microsoft.Maps.Directions', { callback: function(){
                              var directionsManager = new Microsoft.Maps.Directions.DirectionsManager(scope.map);
                            Microsoft.Maps.Events.addHandler(directionsManager, 'directionsError', function(e) { 
                              if(e != null){
                              $('.directions').hide();
                              $('#directions-error').html(e.message).removeClass('hide').addClass('show');           
                            }    });
                            directionsManager.resetDirections();
                            directionsManager.setRequestOptions({routeMode: Microsoft.Maps.Directions.RouteMode.driving});
                            var startWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(scope.latitudeFrom,scope.longitudeFrom) }); 
                            var endWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(scope.latitudeTo, scope.longitudeTo) } );
                            var viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(scope.latitudeFrom,scope.longitudeFrom), new Microsoft.Maps.Location(scope.latitudeTo, scope.longitudeTo));
                            scope.map.setView({ bounds: viewBoundaries});
                            directionsManager.addWaypoint(startWaypoint);
                            directionsManager.addWaypoint(endWaypoint);
                            directionsManager.setRenderOptions({ itineraryContainer: document.getElementById('itineraryDiv') });
                            
                            directionsManager.calculateDirections();
                            } 
                          }); 
                          
                         
                      
                    }
                   
                  });
                  if (angular.isDefined(scope.latitudeTo) && angular.isDefined(scope.longitudeTo) && angular.isDefined(scope.latitudeFrom) && angular.isDefined(scope.longitudeFrom)) { 
                    //console.log("i m latitudeTo "+scope.latitudeTo);
                         var mapOptions = {
                            credentials: 'Ag00iheqslugEVbwF43XtJCoY0tAUdcyXd8XS3oA4l389lpCxFj2vYXknu_B_0ua', 
                            //zoom : 8, 
                            showDashboard: true, 
                            showMapTypeSelector:true, 
                            enableHighDpi: true,
                            showCopyright:false,
                            enableSearchLogo: false,
                            enableClickableLogo: false
                          };
                          scope.map = new Microsoft.Maps.Map(document.getElementById("mapDiv"), mapOptions); 
                          Microsoft.Maps.loadModule('Microsoft.Maps.Directions', { callback: function(){
                              var directionsManager = new Microsoft.Maps.Directions.DirectionsManager(scope.map);
                            Microsoft.Maps.Events.addHandler(directionsManager, 'directionsError', function(e) { 
                              if(e != null){
                              $('.directions').hide();
                              $('#directions-error').html(e.message).removeClass('hide').addClass('show');           
                            }    });
                            directionsManager.resetDirections();
                            directionsManager.setRequestOptions({routeMode: Microsoft.Maps.Directions.RouteMode.driving});
                            var startWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(scope.latitudeTo, scope.longitudeTo) }); 
                            var endWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(scope.latitudeFrom,scope.longitudeFrom) } );
                            var viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(scope.latitudeTo, scope.longitudeTo), new Microsoft.Maps.Location(scope.latitudeFrom, scope.longitudeFrom));
                            scope.map.setView({ bounds: viewBoundaries});
                            directionsManager.addWaypoint(startWaypoint);
                            directionsManager.addWaypoint(endWaypoint);
                            directionsManager.setRenderOptions({ itineraryContainer: document.getElementById('itineraryDiv') });
                            
                            directionsManager.calculateDirections();
                            } 
                          }); 

                  }
                  
                }
               
              $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href");
                //console.log(" i m here"); 
                scope.$evalAsync(getMap());
              });
              var init = function () {  
                scope.$evalAsync(getMap());
              }();
        }  
    };  
    });
});