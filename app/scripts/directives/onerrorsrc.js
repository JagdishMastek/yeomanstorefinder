define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc directive
   * @name storesApp.directive:onErrorSrc
   * @description
   * # onErrorSrc
   */
  angular.module('storesApp.directives.OnErrorSrc', [])
    .directive('onErrorSrc', function () {
      return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });
        }
      };
    });
});
