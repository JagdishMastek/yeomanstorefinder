define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc directive
   * @name storesApp.directive:customCollapse
   * @description
   * # customCollapse
   */
  angular.module('storesApp.directives.CustomCollapse', [])
    .directive('customCollapse', function () {
          return {
              require: '?ngModel',
              scope:{
                  ngModel: '='
              },
              restrict: 'A',
            
              template: '<div class="panel-group" id="{{panelId}}">\
                            <div class="panel panel-default according-markers" ng-repeat-start="marker in ngModel" >\
                                <div class="panel-heading">\
                                    <h3 class="panel-title">\
                                      <a ng-click="toggleCollapsedStates(marker.id)" href=""><i class="indicator pull-right glyphicon glyphicon-plus"></i>{{marker.serviceName.toString().replace("petrolstation", "Petrol station")}}\
                                      </a>\
                                    </h3>\
                                </div>\
                                  <div id="{{panelBaseId}}-{{marker.id}}" data-parent="#{{panelId}}" class="panel-collapse collapse" >\
                                    <div class="panel-body">\
                                    <ul style="list-style:none;">\
                                      <li ng-repeat="cols in marker.openAndClose track by $index" ng-class="{highLightday:cols.highLightDay==true}"><span>{{cols.dayName}}</span> <span>{{cols.openTiming}} </span></li></ul>\
                                      <ul ng-class="{specialOpeningTimes:marker.holidays.length!=0}">\
                                      <li ng-repeat="holidaycols in marker.holidays track by $index" ><span>{{holidaycols.dayName}}</span> <span>{{holidaycols.specialopenTiming}} </span></li></ul>\
                                  </div>\
                                </div>\
                            </div>\
                            <div ng-repeat-end></div>\
                        </div>',
                
                        link: function (scope, el, attrs) {
                        scope.panelBaseId = attrs.collapsePanelBodyId;
                        scope.panelId = attrs.collapsePanelId;
                        scope.toggleCollapsedStates = function(ind){
                            angular.forEach(scope.ngModel, function(value, key){
                                //console.log(JSON.stringify("key==> "+key+" id==>"+ind));
                                if (key == ind)
                                {
                                    scope.ngModel[key].collapsed = !scope.ngModel[key].collapsed;
                                    $("#" + scope.panelBaseId + "-" + ind).collapse('toggle');
                                }
                                else
                                    scope.ngModel[key].collapsed = false;
                            });
                        }

                      scope.toggleChevron=function (e) {
                            $(e.target)
                                .prev('.panel-heading')
                                .find("i.indicator")
                                .toggleClass('glyphicon-minus glyphicon-plus');
                        }
                        $('#accordion').on('hidden.bs.collapse', scope.toggleChevron);
                        $('#accordion').on('shown.bs.collapse', scope.toggleChevron); 
              }
          };
    });
});
