define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc directive
   * @name storesApp.directive:bingMap
   * @description
   * # bingMap
   */
  angular.module('storesApp.directives.BingMap', [])
    .directive('bingMap', function (cfg,$routeParams,$window,LocalStorageService) {
      return {
        template: '<div id="bmaps"></div>',
        replace: true,
        scope:{
          mapOption: '=',
          dbMarkers: '='
        },
        link: function (scope, element, attrs) { 
           function GetMap() {
              scope.$watchCollection("mapOption", function(newValue) {
                  if(angular.isDefined(newValue) && newValue !== null) {
                    scope.mapOption = newValue;
                    scope.$watchCollection("dbMarkers", function(markerValue) {
                      if(angular.isDefined(markerValue) && markerValue !== null) {
                        var latlog = LocalStorageService.getItem("ResultLatLong");//$routeParams.latlang;
                        var latlonArray = latlog.split('::');
                        var selectedLat = latlonArray[0];
                        var selectedlog = latlonArray[1]; 
                        var customIcon = "./images/ListingsPin-01.svg"; 
                        var infoboxLayer = new Microsoft.Maps.EntityCollection();
                        var pinLayer = new Microsoft.Maps.EntityCollection();
                        
                        var map = new Microsoft.Maps.Map(element[0], scope.mapOption);
                        map.entities.clear();
                        pinLayer.clear();
                        
                        var pinInfobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false });
                        infoboxLayer.push(pinInfobox);
                        var locs = [];
                        var offset = new Microsoft.Maps.Point(0, 10);  
                        var i = 1;
                        pinInfobox.setOptions({visible:false});
                        angular.forEach(markerValue, function(records) {  
                            //console.log(records.latitude);
                            var latLon = new Microsoft.Maps.Location(records.latitude, records.longitude);
                            var pushPinOpt = {text:String(i),typeName: "pin" + i,visible:true,icon: customIcon, width:  28, height: 50, textOffset: offset};
                            var pin = new Microsoft.Maps.Pushpin(latLon,pushPinOpt);
                            pin.Title = "";
                            locs.push(pin.getLocation());
                                  
                            var storename = records.storeName.split(' ').join(' ');
                            var storeUrl = records.id;
                            var storeAddress = '<a href="'+storeUrl+'">'+storename+'</a> <br/>';                           
                            pin.Description = storeAddress; //information you want to display in the infobox
                            pinLayer.push(pin); //add pushpin to pinLayer
                            Microsoft.Maps.Events.addHandler(pin, 'click', function(e) {
                                if (e.targetType === "pushpin") {
                                    var pin = e.target;
                                    var pushpinFrameHTML = '<div class="infobox"><div class="infobox_content">{content}<a class="infobox_close" href="javascript:closeInfobox();"><span class="glyphicon glyphicon-remove"></span></a></div></div><div class="infobox_pointer"><img src="./images/pointer_shadow.png"></div>';
                                    var html = "<span class='infobox_title'>" + pin.Title + "</span><br/>" + pin.Description;
                                    
                                    pinInfobox.setOptions({
                                          visible:true,
                                          showCloseButton: true,
                                          height:50,
                                          offset: new Microsoft.Maps.Point(0, 30),
                                          title:pin.Title, description:pin.Description 
                                          //htmlContent: pushpinFrameHTML.replace('{content}',html)
                                    });

                                        //set location of infobox
                                    pinInfobox.setLocation(pin.getLocation());
                                    map.setView({ center: pin.getLocation()});
                                
                              }
                            });
                            Microsoft.Maps.Events.addHandler(map, 'click', function() {
                                pinInfobox.setOptions({visible:false});
                            });
                            
                            i++;
                        });

                      
                        var bestview = Microsoft.Maps.LocationRect.fromLocations(locs);
                        
                        map.setView({ center: bestview.center, zoom: 10 });
                        var geoLocationProvider = new Microsoft.Maps.GeoLocationProvider(map);
                        var mapLatLong = new Microsoft.Maps.Location(selectedLat, selectedlog);
                        //$scope.userMarker(mapLatLong);
                        geoLocationProvider.addAccuracyCircle(mapLatLong, 800, 800, {             
                              polygonOptions: {
                                strokeThickness:1,
                                fillColor: new Microsoft.Maps.Color(125, 252, 188, 0),
                                strokeColor: new Microsoft.Maps.Color(255, 252, 188, 0)
                              }
                        });
                        map.entities.push(pinLayer);
                        map.entities.push(infoboxLayer); 
                        
                        
                        
                         
                       
                      }
                    });
                  }
              });
           } 
           angular.element(document).ready(function () {
          scope.$evalAsync(GetMap());
           });
          $(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
            var target = $(e.target).attr("aria-controls") // activated tab
            if(target=="map"){
                scope.$evalAsync(GetMap());
            }
          });
         /*angular.element($window).bind('resize', function(){
           scope.$evalAsync(GetMap());
         }); */
        } 
     };
   });
});
