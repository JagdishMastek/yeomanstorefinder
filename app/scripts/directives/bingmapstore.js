define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc directive
   * @name storesApp.directive:bingMapStore
   * @description
   * # bingMapStore
   */
  angular.module('storesApp.directives.BingMapStore', [])
    .directive('bingMapStore', function ($window) {
      return {
        template: '<div id="bmaps"></div>',
        replace: true,
        scope:{
          mapOption: '='
        },
        link: function (scope, element, attrs) { 
         scope.map = null;  
         scope.mapOption = null;
         function getMap(){
            scope.$watchCollection("mapOption", function(newValue) {
            if(angular.isDefined(newValue) && newValue !== null) {
              scope.mapOption = newValue;
                // Initialize the map
                scope.map = new Microsoft.Maps.Map(element[0],
                         scope.mapOption);
                // Retrieve the location of the map center 
                var center = scope.map.getCenter();
                // Add a pin to the center of the map, using a custom icon
                var customIcon = "./images/ListingsPin-01.svg";
                var pin = new Microsoft.Maps.Pushpin(center, {icon: customIcon, width: 28, height: 50}); 
                scope.map.entities.push(pin);
              }
            });
            
         }
         scope.$evalAsync(getMap());
         $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href");
                scope.$evalAsync(getMap());
          });
         angular.element($window).bind('resize', function(){
           scope.$evalAsync(getMap());
         }); 
        }
      };
    });
});
